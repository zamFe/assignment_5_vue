

import Vuex from 'vuex'
import Vue from 'vue'
import { getQuestionCategories } from "@/api/categoryAPI";
import { getQuestions, getToken } from "@/api/gameAPI";
import { getScores, setScore, updateScore } from "@/api/scoreAPI";

Vue.use(Vuex)

// Sort Fuctions Are Giving Items with HighScore both That and Score
function sortScoreList(scoreList) {
    scoreList.sort(function (a, b) {
        let bScore = b.score;
        let aScore = a.score;

        if ('highScore' in b && !('score' in b)) {
            bScore = b.highScore;
        }

        if ('highScore' in a && !('score' in a)) {
            aScore = a.highScore;
        }

        return bScore - aScore;
    });
}

export default new Vuex.Store({

    state: {
        // Data To Pass From Start to Trivia Screen
        // with Default Values to Fallback on.

        loadingCharacters: true,
        error: null,
        userId: 0,
        nameOfUser: 'NO NAME',
        numberOfQuestions: 1,
        difficultyLevel: "easy", // https://opentdb.com/api.php?amount=10&difficulty=hard
        questionCategory: 9, //General Knowledge //https://opentdb.com/api.php?amount=10&category=22
        categoryList: [],

        // Question Set
        questionList: [],
        loadingQuestions: true,
        errorQuestion: null,

        usersAnswers: [],

        // Trivia Screen
        questionIndex: 0,

        // Trivia Body
        correctAnswers: 0,

        // Trivia Screen Routing Lock
        allowTriviaRoute: false,

        // Scoreboard Routing Lock
        allowScoreboardRoute: false,

        // Result Routing Lock
        allowResultRoute: false,

        // Score Set
        scoreList: [],
        loadingScore: true,
        errorScore: null,

        // Score Push Set
        loadingScore2: true,
        errorScore2: null,

        // Score Patch Set
        errorScorePatch: null,

        // TOKEN
        loadingToken: true,
        token: null,
        tokenError: null,
    },
    mutations: {
        incrementQuestionIndex (state) {
            if (state.questionIndex < state.questionList.length - 1) {
                state.questionIndex++
            } else {
                state.questionIndex = -1
            }
        },

        incrementCorrectAnswerC (state) {
            state.correctAnswers++
        },

        resetForNewGame(state) {
            // In the Case Users Plays Another Round Because Our Game Is Just so Damn Good!
            state.questionIndex = 0;
            state.correctAnswers = 0;
            state.usersAnswers = [];
        },

        updateUserPreferences(state, payload) {

            state.nameOfUser = payload.nameOfUser
            state.questionCategory = payload.questionCategory;
            state.numberOfQuestions = payload.numberOfQuestions;
            state.difficultyLevel = payload.difficultyLevel;

            // In the Case Users Plays Another Round Because Our Game Is Just so Damn Good!
            state.questionIndex = 0;
            state.correctAnswers = 0;
            state.usersAnswers = [];
        },

        triviaLock(state, payload) {
          state.allowTriviaRoute = payload;
        },

        scoreboardLock(state, payload) {
            state.allowScoreboardRoute = payload;
        },

        resultLock(state, payload) {
            state.allowResultRoute = payload;
        },

        setCategoryList: (state, payload) => {
            state.categoryList = payload;
        },

        setLoading: (state, payload) => {
            state.loadingCharacters = payload;
        },

        setError: (state, payload) => {
            state.error = payload;
        },

        setQuestionList: (state, payload) => {
            state.questionList = payload;
        },

        setLoadingQuestion: (state, payload) => {
            state.loadingQuestions = payload;
        },

        setErrorQuestion: (state, payload) => {
            state.errorQuestion = payload;
        },

        setScoreList: (state, payload) => {
            state.scoreList = payload;
        },

        addToScoreList: (state, payload) => {
            state.scoreList.push(payload);
            sortScoreList(state.scoreList)
        },

        UpdateScoreList: (state, payload) => {
            for (let i = 0; i < state.scoreList.length; i++) {
                if (state.scoreList[i].id === payload.id) {
                    state.scoreList.splice(i, 1);
                    break;
                }
            }
            state.scoreList.splice(0, 0, payload);
            sortScoreList(state.scoreList)
        },

        setLoadingScore: (state, payload) => {
            state.loadingScore = payload;
        },

        setErrorScore: (state, payload) => {
            state.errorScore = payload;
        },

        setLoadingScore2: (state, payload) => {
            state.loadingScore2 = payload;
        },

        setErrorScore2: (state, payload) => {
            state.errorScore2 = payload;
        },

        setErrorScorePatch: (state, payload) => {
            state.errorScorePatch = payload;
        },

        addUserAnswer: (state, payload) => {
            state.usersAnswers.push(payload.clicked);
        },

        setToken: (state, payload) => {
            state.token = payload;
            state.loadingToken = false;
        },

        tokenError: (state, payload) => {
            state.tokenError = payload;
        },

        updateUserId: (state, payload) => {
            state.userId = payload.userID;
        },
    },
    actions: {
        async getCategories({commit}) {
            const [error, characters] = await getQuestionCategories();
            commit('setCategoryList', characters)
            commit('setError', error)
            commit('setLoading', false)
        },

        async getQuestionSet({commit, state}) {
            commit('setLoadingQuestion', true)
            const [error, questions] = await getQuestions(state.numberOfQuestions, state.questionCategory, state.difficultyLevel, state.token);
            questions.map(e => e.incorrect_answers.push(e.correct_answer))
            questions.map(e => e.incorrect_answers.sort())
            commit('setQuestionList', questions)
            commit('setErrorQuestion', error)
            commit('setLoadingQuestion', false)
        },

        async getHighScores({commit}) {
            const [error, scores] = await getScores();
            sortScoreList(scores);
            commit('setScoreList', scores)
            commit('setErrorScore', error)
            commit('setLoadingScore', false)
        },

        async postHighScore({commit, state}) {
            const [error, scores] = await setScore(state.nameOfUser, (state.correctAnswers * 10));
            commit('addToScoreList', scores)
            commit('setErrorScore2', error)
            commit('setLoadingScore2', false)
        },

        async updateHighScore({ commit, state }) {
            const [error, scores] = await updateScore(state.userId, (state.correctAnswers * 10));
            commit('UpdateScoreList', scores)
            commit('setErrorScorePatch', error)

        },

        async getAPIToken({ commit }) {
            const [error, token] = await getToken();
            commit('setToken', token)
            commit('tokenError', error)

        },
    }



    /*state: {
        name: '',
        score: '',
        questionSet: '',
        diffuculty: ''
    },
    getters: {
        score: state => {
            return state.score
        }
    },
    mutations: {
        setLoadQuestion: (state, payload) {
            state.loadQuestions = payload
        }
    },
    actions: {
        async fetchQuestions({ commit }) {
            const [ error, questions ] = API.get()
            commit('setError', error)
            commit('setLoadQuestion', questions)
            commit('setLoading', false)
        }
    }*/
})



