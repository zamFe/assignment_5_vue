export async function setScore(name, score) { // username, score
    try {
        const scores = await fetch("https://noroff-assignment-5-api.herokuapp.com/trivia", {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                'x-api-key': 'biq?BnhbCE&h3A8dpTz8p6!?NAf&HtEx3yE3TnosGcoH&hRKPDRPDhBmn!J$5Y79'
            },
            body: JSON.stringify({
                username: name,
                score: score
            })
        }).then(response => response.json())
        return [null, scores]
    } catch (e) {
        return [e.message, {}]
    }
}

export async function updateScore(id, score) {
    try {
        const response = await fetch(`https://noroff-assignment-5-api.herokuapp.com/trivia/${id}`, {
            method: 'PATCH',
            headers: {
                'Content-Type': 'application/json',
                'x-api-key': 'biq?BnhbCE&h3A8dpTz8p6!?NAf&HtEx3yE3TnosGcoH&hRKPDRPDhBmn!J$5Y79'
            },
            body: JSON.stringify({
                score: score
            })
        }).then(response => response.json())
        return [null, response]
    } catch (e) {
        return [e.message, {}]
    }
}

export async function getScores() {
    try {
        const scores = await fetch("https://noroff-assignment-5-api.herokuapp.com/trivia")
            .then(response => response.json())
        return [null, scores]
    } catch (e) {
        return [e.message, {}]
    }
}