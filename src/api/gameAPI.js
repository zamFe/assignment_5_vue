//https://opentdb.com/api.php?amount=10&category=23&difficulty=easy

export async function getToken() {
    try {
        const {token} = await fetch("https://opentdb.com/api_token.php?command=request")
            .then(response => response.json())
        return [null, token]
    } catch (e) {
        return [e.message, {}]
    }
}

export async function getQuestions(count, categoryID, difficulty, token) {
    try {

        // Count Good - Min / Max Fixed
        // Category Check For Any -> -1

        let BASE_URL = "https://opentdb.com/api.php?";
        BASE_URL += `amount=${count}`

        if (categoryID !== "-1") {
            BASE_URL += `&category=${categoryID}`
        }

        if (difficulty !== "-1") {
            BASE_URL += `&difficulty=${difficulty}`
        }

        BASE_URL += `&token=${token}`;

        const {results} = await fetch(BASE_URL)
            .then(response => response.json())
        return [null, results]
    } catch (e) {
        return [e.message, {}]
    }
}