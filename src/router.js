import VueRouter from 'vue-router';
import Vue from 'vue';
import Trivia from '@/components/Trivia'
import Scoreboard from '@/components/Score'
import Start from '@/components/Start'
import store from "@/store";
import Result from "@/components/Result";

Vue.use(VueRouter)

const routes = [{
        path: "/",
        component: Start,
        alias: '/start',
    },
    {
        path: "/game",
        //alias: '/',
        component: Trivia,
        beforeEnter: (to, from, next) => {
            if (store.state.allowTriviaRoute) {
                next()
            } else {
                next(false)
            }
        }
    },
    {
        path: "/result",
        component: Result,
        beforeEnter: (to, from, next) => {
            if (store.state.allowResultRoute) {
                next()
            } else {
                next(false)
            }
        }
    },
    {
        path: "/score",
        component: Scoreboard,
        beforeEnter: (to, from, next) => {
            if (store.state.allowScoreboardRoute) {
                next()
            } else {
                next(false)
            }
        }
    }
]

export default new VueRouter({ routes })